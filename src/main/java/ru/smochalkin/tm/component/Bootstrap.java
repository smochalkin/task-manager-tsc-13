package ru.smochalkin.tm.component;

import ru.smochalkin.tm.api.controller.ICommandController;
import ru.smochalkin.tm.api.controller.IProjectController;
import ru.smochalkin.tm.api.controller.IProjectTaskController;
import ru.smochalkin.tm.api.controller.ITaskController;
import ru.smochalkin.tm.api.repository.ICommandRepository;
import ru.smochalkin.tm.api.repository.IProjectRepository;
import ru.smochalkin.tm.api.repository.ITaskRepository;
import ru.smochalkin.tm.api.service.ICommandService;
import ru.smochalkin.tm.api.service.IProjectService;
import ru.smochalkin.tm.api.service.IProjectTaskService;
import ru.smochalkin.tm.api.service.ITaskService;
import ru.smochalkin.tm.constant.ArgumentConst;
import ru.smochalkin.tm.constant.TerminalConst;
import ru.smochalkin.tm.controller.CommandController;
import ru.smochalkin.tm.controller.ProjectController;
import ru.smochalkin.tm.controller.ProjectTaskController;
import ru.smochalkin.tm.controller.TaskController;
import ru.smochalkin.tm.repository.CommandRepository;
import ru.smochalkin.tm.repository.ProjectRepository;
import ru.smochalkin.tm.repository.TaskRepository;
import ru.smochalkin.tm.service.CommandService;
import ru.smochalkin.tm.service.ProjectService;
import ru.smochalkin.tm.service.ProjectTaskService;
import ru.smochalkin.tm.service.TaskService;
import ru.smochalkin.tm.util.TerminalUtil;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository ,taskRepository);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    public void start(final String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        parseArgs(args);
        process();
    }

    public void parseArgs(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        commandController.exit();
    }

    public void parseArg(final String arg) {
        switch (arg) {
            case ArgumentConst.HELP: commandController.showHelp(); break;
            case ArgumentConst.ABOUT: commandController.showAbout(); break;
            case ArgumentConst.VERSION: commandController.showVersion(); break;
            case ArgumentConst.INFO: commandController.showInfo(); break;
            case ArgumentConst.COMMANDS: commandController.showCommands(); break;
            case ArgumentConst.ARGUMENTS: commandController.showArguments(); break;
            default: commandController.showErrorArgument();
        }
    }

    public void process() {
        String command = "";
        while (!TerminalConst.EXIT.equals(command)) {
            System.out.print("Enter command: ");
            command = TerminalUtil.nextLine();
            parseCommand(command);
            System.out.println();
        }
    }

    public void parseCommand(final String command) {
        switch (command) {
            case TerminalConst.HELP: commandController.showHelp(); break;
            case TerminalConst.ABOUT: commandController.showAbout(); break;
            case TerminalConst.VERSION: commandController.showVersion(); break;
            case TerminalConst.INFO: commandController.showInfo(); break;
            case TerminalConst.COMMANDS: commandController.showCommands(); break;
            case TerminalConst.ARGUMENTS: commandController.showArguments(); break;
            case TerminalConst.PROJECT_CREATE: projectController.createProject(); break;
            case TerminalConst.PROJECT_LIST: projectController.showProjects(); break;
            case TerminalConst.PROJECT_CLEAR: projectController.clearProjects(); break;
            case TerminalConst.PROJECT_SHOW_BY_ID: projectController.showById(); break;
            case TerminalConst.PROJECT_SHOW_BY_NAME: projectController.showByName(); break;
            case TerminalConst.PROJECT_SHOW_BY_INDEX: projectController.showByIndex(); break;
            case TerminalConst.PROJECT_REMOVE_BY_ID: projectTaskController.removeProjectById(); break;
            case TerminalConst.PROJECT_REMOVE_BY_NAME: projectTaskController.removeProjectByName(); break;
            case TerminalConst.PROJECT_REMOVE_BY_INDEX: projectTaskController.removeProjectByIndex(); break;
            case TerminalConst.PROJECT_UPDATE_BY_ID: projectController.updateById(); break;
            case TerminalConst.PROJECT_UPDATE_BY_INDEX: projectController.updateByIndex(); break;
            case TerminalConst.PROJECT_START_BY_ID: projectController.startById(); break;
            case TerminalConst.PROJECT_START_BY_NAME: projectController.startByName(); break;
            case TerminalConst.PROJECT_START_BY_INDEX: projectController.startByIndex(); break;
            case TerminalConst.PROJECT_COMPLETE_BY_ID: projectController.completeById(); break;
            case TerminalConst.PROJECT_COMPLETE_BY_NAME: projectController.completeByName(); break;
            case TerminalConst.PROJECT_COMPLETE_BY_INDEX: projectController.completeByIndex(); break;
            case TerminalConst.PROJECT_STATUS_UPDATE_BY_ID: projectController.updateStatusById(); break;
            case TerminalConst.PROJECT_STATUS_UPDATE_BY_NAME: projectController.updateStatusByName(); break;
            case TerminalConst.PROJECT_STATUS_UPDATE_BY_INDEX: projectController.updateStatusByIndex(); break;
            case TerminalConst.TASK_CREATE: taskController.createTask(); break;
            case TerminalConst.TASK_LIST: taskController.showTasks(); break;
            case TerminalConst.TASK_CLEAR: taskController.clearTasks(); break;
            case TerminalConst.TASK_SHOW_BY_ID: taskController.showById(); break;
            case TerminalConst.TASK_SHOW_BY_NAME: taskController.showByName(); break;
            case TerminalConst.TASK_SHOW_BY_INDEX: taskController.showByIndex(); break;
            case TerminalConst.TASK_REMOVE_BY_ID: taskController.removeById(); break;
            case TerminalConst.TASK_REMOVE_BY_NAME: taskController.removeByName(); break;
            case TerminalConst.TASK_REMOVE_BY_INDEX: taskController.removeByIndex(); break;
            case TerminalConst.TASK_UPDATE_BY_ID: taskController.updateById(); break;
            case TerminalConst.TASK_UPDATE_BY_INDEX: taskController.updateByIndex(); break;
            case TerminalConst.TASK_START_BY_ID: taskController.startById(); break;
            case TerminalConst.TASK_START_BY_NAME: taskController.startByName(); break;
            case TerminalConst.TASK_START_BY_INDEX: taskController.startByIndex(); break;
            case TerminalConst.TASK_COMPLETE_BY_ID: taskController.completeById(); break;
            case TerminalConst.TASK_COMPLETE_BY_NAME: taskController.completeByName(); break;
            case TerminalConst.TASK_COMPLETE_BY_INDEX: taskController.completeByIndex(); break;
            case TerminalConst.TASK_STATUS_UPDATE_BY_ID: taskController.updateStatusById(); break;
            case TerminalConst.TASK_STATUS_UPDATE_BY_NAME: taskController.updateStatusByName(); break;
            case TerminalConst.TASK_STATUS_UPDATE_BY_INDEX: taskController.updateStatusByIndex(); break;
            case TerminalConst.TASK_BIND_BY_PROJECT_ID: projectTaskController.bindTaskByProjectId(); break;
            case TerminalConst.TASK_UNBIND_BY_PROJECT_ID: projectTaskController.unbindTaskByProjectId(); break;
            case TerminalConst.TASK_SHOW_BY_PROJECT_ID: projectTaskController.showTasksByProjectId(); break;
            case TerminalConst.EXIT: commandController.exit(); break;
            default: commandController.showErrorCommand();
        }
    }

}
