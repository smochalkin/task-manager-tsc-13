package ru.smochalkin.tm.controller;

import ru.smochalkin.tm.api.controller.ICommandController;
import ru.smochalkin.tm.api.service.ICommandService;
import ru.smochalkin.tm.model.Command;
import ru.smochalkin.tm.util.NumberUtil;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getCommands();
        for (final Command command : commands) {
            System.out.println(command);
        }
    }

    @Override
    public void showArguments() {
        System.out.println("[ARGUMENTS]");
        final Command[] commands = commandService.getCommands();
        for (final Command command : commands) {
            showCommandValue(command.getArgument());
        }
    }

    @Override
    public void showCommands() {
        System.out.println("[COMMANDS]");
        final Command[] commands = commandService.getCommands();
        for (final Command command : commands) {
            showCommandValue(command.getName());
        }
    }

    @Override
    public void showCommandValue(String value) {
        if(value == null || value.isEmpty()) return;
        System.out.println(value);
    }

    @Override
    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Developer: Sergey Mochalkin");
        System.out.println("smochalkin@gmail.com");
    }

    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.7.0");
    }

    @Override
    public void showInfo() {
        System.out.println("[INFO]");
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + processors);
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

    @Override
    public void showErrorCommand() {
        System.out.println("Command not found...");
    }

    @Override
    public void showErrorArgument() {
        System.err.println("Argument not supported...");
        System.exit(1);
    }

    @Override
    public void exit() {
        System.exit(0);
    }

}
