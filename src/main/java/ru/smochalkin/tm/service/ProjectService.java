package ru.smochalkin.tm.service;

import ru.smochalkin.tm.api.repository.IProjectRepository;
import ru.smochalkin.tm.api.service.IProjectService;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.model.Project;

import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(String name) {
        if (name == null || name.isEmpty()) return;
        Project project = new Project();
        project.setName(name);
        projectRepository.add(project);
    }

    @Override
    public void create(String name, String description) {
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
    }

    @Override
    public void add(Project project) {
        if (project == null) return;
        projectRepository.add(project);
    }

    @Override
    public void remove(Project project) {
        if (project == null) return;
        projectRepository.remove(project);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project findById(String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.findById(id);
    }

    @Override
    public Project findByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.findByName(name);
    }

    @Override
    public Project findByIndex(Integer index) {
        if (index == null || index < 0) return null;
        if (index >= projectRepository.getCount()) return null;
        return projectRepository.findByIndex(index);
    }

    @Override
    public Project removeById(String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.removeById(id);
    }

    @Override
    public Project removeByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.removeByName(name);
    }

    @Override
    public Project removeByIndex(Integer index) {
        if (index == null || index < 0) return null;
        if (index >= projectRepository.getCount()) return null;
        return projectRepository.removeByIndex(index);
    }

    @Override
    public Project updateById(String id, String name, String desc) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        return projectRepository.updateById(id, name, desc);
    }

    @Override
    public Project updateByIndex(Integer index, String name, String desc) {
        if (index == null || index < 0) return null;
        if (index >= projectRepository.getCount()) return null;
        if (name == null || name.isEmpty()) return null;
        return projectRepository.updateByIndex(index, name, desc);
    }

    @Override
    public Project updateStatusById(String id, Status status) {
        if (id == null || id.isEmpty()) return null;
        if (status == null) return null;
        return projectRepository.updateStatusById(id, status);
    }

    @Override
    public Project updateStatusByName(String name, Status status) {
        if (name == null || name.isEmpty()) return null;
        if (status == null) return null;
        return projectRepository.updateStatusByName(name, status);
    }

    @Override
    public Project updateStatusByIndex(Integer index, Status status) {
        if (index == null || index < 0) return null;
        if (index >= projectRepository.getCount()) return null;
        if (status == null) return null;
        return projectRepository.updateStatusByIndex(index, status);
    }

}
